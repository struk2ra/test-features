import React, { PropTypes } from 'react';

const SelectSwitcher = ({ currentValue, values, changeHandler }) => {
  return (
    <select onChange={changeHandler} value={currentValue}>
      {values.map((value) => <option key={value} value={value}>{value}</option>)}
    </select>
  );
};

SelectSwitcher.propTypes = {
  currentValue: PropTypes.number.isRequired,
  values: PropTypes.array.isRequired,
  changeHandler: PropTypes.func.isRequired
};

export default SelectSwitcher;
