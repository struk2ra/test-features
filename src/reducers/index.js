import { combineReducers } from 'redux';
import {
  CHANGE_CONFIG,
  RECEIVE_CONFIG,
  REQUEST_CONFIG
} from '../actions';

export function config(state = {
  config: 0,
  configValues: [0, 1, 2],
  isLoadingConfig: true
}, action) {
  switch (action.type) {
    case REQUEST_CONFIG:
      return {
        ...state,
        isLoadingConfig: true
      };
    case RECEIVE_CONFIG:
      return {
        ...state,
        isLoadingConfig: false,
        config: action.config
      };
    case CHANGE_CONFIG:
      return {
        ...state,
        config: parseInt(action.config, 10),
        isLoadingConfig: false
      };
    default:
      return state;
  }
}

export function featureA(state = { foo: 'A' }, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export function featureB(state = { foo: 'B' }, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default combineReducers({ config, featureA, featureB });
