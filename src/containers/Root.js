import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import view from '../reducers';
import { FeatureViewer } from './';


const store = createStore(view, applyMiddleware(thunk));

class Root extends Component {
  render() {
    return (
      <Provider store={store}>
        <FeatureViewer />
      </Provider>
    );
  }
}

export default Root;
