import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { loadConfig, changeConfig } from '../actions';
import { FeatureA, FeatureB } from './';
import { SelectSwitcher } from '../components';

class FeatureViewer extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(loadConfig());
  }

  changeHandler(e) {
    const { dispatch } = this.props;
    const newConfigValue = e.target.value;
    dispatch(changeConfig(newConfigValue));
  }

  render() {
    const { config, configValues } = this.props;
    return (
      <div>
        <h1>Feature Viewer</h1>
        <SelectSwitcher
          values={configValues}
          currentValue={config}
          changeHandler={this.changeHandler.bind(this)}
        />
        {config === 1 && <FeatureA />}
        {config === 2 && <FeatureB />}
      </div>
    );
  }
}

FeatureViewer.propTypes = {
  config: PropTypes.number.isRequired,
  configValues: PropTypes.array.isRequired,
  dispatch: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return state.config;
}

export default connect(mapStateToProps)(FeatureViewer);
