import fetch from 'isomorphic-fetch';

export const LOAD_CONFIG = 'LOAD_CONFIG';
export const CHANGE_CONFIG = 'CHANGE_CONFIG';
export const RECEIVE_CONFIG = 'RECEIVE_CONFIG';
export const REQUEST_CONFIG = 'REQUEST_CONFIG';

export function receiveConfig(config) {
  return {
    type: RECEIVE_CONFIG,
    config
  };
}

export function requestConfig() {
  return {
    type: REQUEST_CONFIG
  };
}

export function loadConfig() {
  return dispatch => {
    dispatch(requestConfig());
    return fetch('http://localhost:5000/config.json')
      .then(response => response.json())
      .then(json => dispatch(receiveConfig(json.feature)));
  };
}

export function changeConfig(config) {
  return {
    type: CHANGE_CONFIG,
    config
  };
}
