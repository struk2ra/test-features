import expect from 'expect';
import nock from 'nock';
import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import * as actions from '../../src/actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('config actions', () => {
  it('should create RECEIVE_CONFIG action', () => {
    expect(actions.receiveConfig(0)).toEqual({
      type: actions.RECEIVE_CONFIG,
      config: 0
    });
  });

  it('should create REQUEST_CONFIG action', () => {
    expect(actions.requestConfig()).toEqual({
      type: actions.REQUEST_CONFIG
    });
  });

  it('should create CHANGE_CONFIG action', () => {
    expect(actions.changeConfig(2)).toEqual({
      type: actions.CHANGE_CONFIG,
      config: 2
    });
  });

  it('should create RECEIVE_CONFIG action when loading config has been done', () => {
    nock('http://localhost:5000/')
      .get('/config.json')
      .reply(200, { feature: 1 });

    const expectedActions = [
      { type: actions.REQUEST_CONFIG },
      { type: actions.RECEIVE_CONFIG, config: 1 }
    ];

    const store = mockStore({});

    return store.dispatch(actions.loadConfig())
      .then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
  });
});
