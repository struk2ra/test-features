import expect from 'expect';
import { config, featureA, featureB } from '../../src/reducers';

const initialConfigState = {
  config: 0,
  configValues: [0, 1, 2],
  isLoadingConfig: true
};

describe('reducers', () => {
  describe('config', () => {
    it('should provide the initial state', () => {
      expect(config(undefined, {})).toExist();
      expect(config(undefined, {})).toEqual(initialConfigState);
    });

    it('should handle REQUEST_CONFIG action', () => {
      expect(config(undefined, { type: 'REQUEST_CONFIG' }))
        .toEqual({
          ...initialConfigState,
          isLoadingConfig: true
        });
    });

    it('should handle RECEIVE_CONFIG action', () => {
      expect(config(undefined, { type: 'RECEIVE_CONFIG', config: 2 }))
        .toEqual({
          ...initialConfigState,
          config: 2,
          isLoadingConfig: false
        });
    });

    it('should handle CHANGE_CONFIG action', () => {
      expect(config(undefined, { type: 'CHANGE_CONFIG', config: 1 }))
        .toEqual({
          ...initialConfigState,
          config: 1,
          isLoadingConfig: false
        });
    });
  });

  describe('featureA', () => {
    it('should provied the initial state', () => {
      expect(featureA(undefined, {})).toExist();
    });
  });

  describe('featureB', () => {
    it('should provied the initial state', () => {
      expect(featureB(undefined, {})).toExist();
    });
  });
});
